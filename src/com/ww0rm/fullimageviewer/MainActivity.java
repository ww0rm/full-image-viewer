package com.ww0rm.fullimageviewer;

import java.io.File;

import android.os.Bundle;
import android.app.Activity;

public class MainActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		File cacheDir = new File(android.os.Environment.getExternalStorageDirectory(), "Pikabu");
		File file = new File(cacheDir, "test.jpg");
		
		FullImageViewer image = new FullImageViewer(getBaseContext());
		image.setImage(file);
		
		setContentView(image);
	}
}
