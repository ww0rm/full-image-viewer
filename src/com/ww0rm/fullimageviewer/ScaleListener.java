package com.ww0rm.fullimageviewer;

import android.view.ScaleGestureDetector;

public class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
	private FullImageViewer view;

	public ScaleListener(FullImageViewer view) {
		this.view = view;
	}
	
	@Override
	public boolean onScale(ScaleGestureDetector detector) {
		float scale = detector.getPreviousSpan() - detector.getCurrentSpan();
		
		this.view.scaleBitmap(scale);
		return super.onScale(detector);
	}
}
