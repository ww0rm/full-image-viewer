package com.ww0rm.fullimageviewer;

import java.io.File;
import java.io.IOException;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.GestureDetector.OnGestureListener;
import android.view.ScaleGestureDetector.OnScaleGestureListener;
import android.view.GestureDetector;

public class FullImageViewer extends View {

	private int      canvasSizeX;
	private int      canvasSizeY;
	private Paint    mBitmapPaint;
	private Paint    paint;
	private Paint    clearPaint;
	private Bitmap   mBitmap;
	private File     file;
	private Bitmap[] bitmaps = new Bitmap[64];
	private Canvas   mCanvas;
	private Context  context;
	private int      bmpCount;
	private int      y = 0;
	private int      x = 0;
	
	protected ScaleGestureDetector   mScaleDetector;
	protected GestureDetector        mGestureDetector;
	protected OnGestureListener      mGestureListener;
	protected OnScaleGestureListener mScaleListener;
	private float scaleSizeX;
	private float scaleSizeY;

	public FullImageViewer(Context context) {
		super(context);
		this.context = context;
		
		init();
	}

	public FullImageViewer(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		
		init();
	}
	
	public void init() {
		mGestureListener = getGestureListener();
		mScaleListener = getScaleListener();

		mScaleDetector = new ScaleGestureDetector(getContext(), mScaleListener);
		mGestureDetector = new GestureDetector(getContext(), mGestureListener, null, true);
		
		this.setBackgroundColor(0x00000000);
		
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();

        canvasSizeX = metrics.widthPixels;
        canvasSizeY = metrics.heightPixels;
        
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setColor(0xff000000);
        paint.setStyle(Paint.Style.FILL);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        
        clearPaint = new Paint();
        //clearPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        clearPaint.setColor(0xff000000);
        clearPaint.setStyle(Paint.Style.FILL);
        
        mBitmap = Bitmap.createBitmap(canvasSizeX, canvasSizeY, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        
    	mCanvas.drawRect(0, 0, canvasSizeX, canvasSizeY, clearPaint);
	}
	
    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
    	//canvas.scale(scaleSizeX, scaleSizeY);
    }
    
    public void setImage(File f) {
    	this.file = f;
    	
    	try {
			cropFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public void moveBitmap(float x, float y) {
    	this.x += x;
    	this.y += y;
    	
    	mCanvas.drawRect(0, 0, canvasSizeX, canvasSizeY, clearPaint);

        int i = 0;
        Log.v("count", this.bmpCount+"");
        for(i=0; i<this.bmpCount; i++) {
        	int dx = (canvasSizeX - this.bitmaps[i].getWidth()) / 2;
        	int dy = i * this.bitmaps[i].getHeight() + this.y;
        	
        	if(canvasSizeX < this.bitmaps[i].getWidth()) dx += this.x;
        	
            mCanvas.drawBitmap(this.bitmaps[i], dx, dy, this.paint);
        }
        
        invalidate();
    }
    
    public void scaleBitmap(float scale) {
    	scaleSizeX += scale;
    	scaleSizeY += scale;
    	mCanvas.scale(scaleSizeX, scaleSizeY);
    	invalidate();
    }
    
	private void cropFile() throws IOException {
		BitmapRegionDecoder brd = BitmapRegionDecoder.newInstance(file.getAbsolutePath(), true);
		
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds    = true;
        BitmapFactory.decodeFile(this.file.getAbsolutePath(), o);
        
        int i = 0;
        int c = o.outHeight / 2048;
        
        for(i=0; i<c; i++) {       
    		Rect rect = new Rect(0 , i * 2048, o.outWidth, (i + 1) * 2048);
    		
    		BitmapFactory.Options options = new BitmapFactory.Options();
    		options.inSampleSize = 1;
            
    		Bitmap bmpTemp  = brd.decodeRegion(rect, options);
            this.bitmaps[i] = bmpTemp;
        }
        
        this.bmpCount = i;
        
        for(i=0; i<this.bmpCount; i++) {
        	int x = (canvasSizeX - this.bitmaps[i].getWidth()) / 2;
        	int y = i * 2048;
            mCanvas.drawBitmap(this.bitmaps[i], x, y, this.paint);
        }
    }

	protected OnGestureListener getGestureListener() {
		return new GestureListener(this);
	}

	protected OnScaleGestureListener getScaleListener() {
		return new ScaleListener(this);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		mScaleDetector.onTouchEvent(event);
		if(!mScaleDetector.isInProgress()) mGestureDetector.onTouchEvent(event);
		
		int action = event.getAction();
		switch (action & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_UP:

				break;
		}
		return true;
	}
}
